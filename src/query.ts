export type Query = Disjunction | Conjunction | Equality | QueryText;

export interface Disjunction {
  op: "or";
  args: Query[];
}

export interface Conjunction {
  op: "and";
  args: Query[];
}

export interface Equality {
  op: "eq";
  field: string;
  value: EqTo;
}

export type EqTo = DisjunctiveText | ConjunctiveText | EqToText;

export interface DisjunctiveText {
  op: "or";
  args: EqTo[];
}

export interface ConjunctiveText {
  op: "and";
  args: EqTo[];
}

export interface EqToText {
  op: "text";
  value: string;
}

export interface QueryText {
  op: "text",
  value: string;
}

