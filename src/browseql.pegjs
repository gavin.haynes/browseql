Query = _ d:MaybeDisjunction _ { return d; }

MaybeDisjunction = Disjunction
	/ x0:Term { return x0; }

Disjunction = x0:Term xs:(_ Or _ x:Term { return x; })+ { return {"op": "or", "args": [x0, ...xs]}; }

Term = Conjunction

Conjunction = x0:Factor xs:(_ And _ x:Factor { return x; })+ { return { "op": "and", "args": [x0, ...xs]}; }
	/ x0:Factor { return x0; }

Factor = Eq
  / x:Text { return { "op": "text", "value": x }; }
  / "(" _ q:Query _ ")" { return q; }

Eq = l:Ident _ "=" _ r:EqTo { return { "op": "eq", field: l, "value": r }; }

EqTo = EqToText
  / "(" _ x:MaybeDisjunctiveText _ ")" { return x; }

MaybeDisjunctiveText = DisjunctiveText / MaybeConjunctiveText

DisjunctiveText = x0:MaybeConjunctiveText xs:(_ Or _ x:MaybeConjunctiveText { return x; })+ { return {"op": "or", "args": [x0, ...xs]}; }

MaybeConjunctiveText = ConjunctiveText / EqToText

ConjunctiveText = x0:EqToText xs:(_ And _ x:EqToText { return x; })+ { return {"op": "and", "args": [x0, ...xs]}; }

EqToText = "(" _ x:MaybeDisjunctiveText _ ")" { return x; }
  / x:Text { return {"op": "text", "value": x}; }

Text = Ident
  / Quote ([^"\\]+ / EscapeSeq )+ Quote { return JSON.parse(text()); }

Ident = [a-zA-Z0-9$_.*]+ { return text(); }

EscapeSeq = "\\" .

Or = "||" / "|"

And = "&" / "&&"

Integer "integer"
  = _ [0-9]+ { return parseInt(text(), 10); }

_ "whitespace"
  = [ \t\n\r]*

Quote = '"';
