import * as lodash from "lodash";
import { Query, EqTo } from "./query";
import { parse } from "./browseql";
import { flattenObject } from "./flattenObject";

interface Record {
   [key: string]: any;
}

export function createFilter(query: string) {
  return createFilterFromAst(parse(query));
}

function tokenMatch(fieldToken: string, queryToken: string) {
  return fieldToken.toLowerCase().startsWith(queryToken.toLowerCase());
}

function createFilterFromAst(ast: Query) {
  return (record: Record) => {
    if (ast.op === "text") {
      const flatRecord = flattenObject(record);
      for (const fieldValue of lodash.values(flatRecord)) {
        const fieldTokens = new String(fieldValue).split(" ");
        for (const token of fieldTokens) {
          if (tokenMatch(token, ast.value)) {
            return true;
          }
        }
      }
      return false;
    }
    if (ast.op === "eq") {
      function filterEq(fieldTokens: string[], eqTo: EqTo) {
        if (eqTo.op === "text") {
          for (const token of fieldTokens) {
            if (tokenMatch(token, eqTo.value)) {
              return true;
            }
          }
          return false;
        }
        if (eqTo.op === "or") {
          for (const arg of eqTo.args) {
            if (filterEq(fieldTokens, arg)) {
              return true;
            }
          }
          return false;
        }
        if (eqTo.op === "and") {
          for (const arg of eqTo.args) {
            if (!filterEq(fieldTokens, arg)) {
              return false;
            }
          }
          return true;
        }
      }
      const flatRecord = flattenObject(record);
      const fieldValue: any = flatRecord[ast.field];
      const fieldTokens = new String(fieldValue).split(" ");
      return filterEq(fieldTokens, ast.value);
    }
    if (ast.op === "or") {
      const filters = ast.args.map(createFilterFromAst);
      for (const filter of filters) {
        if (filter(record)) {
          return true;
        }
      }
      return false;
    }
    if (ast.op === "and") {
      const filters = ast.args.map(createFilterFromAst);
      for (const filter of filters) {
        if (!filter(record)) {
          return false;
        }
      }
      return true;
    }
    return false;
  }
}
