"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function flattenObject(ob) {
    return ob;
    /*
    cancelled due to performance issues
    const toReturn: { [key: string]: string } = {};

    for (let i in ob) {
        if (!ob.hasOwnProperty(i)) continue;

        if ((typeof ob[i]) == 'object' && ob[i] !== null) {
            const flatObject = flattenObject(ob[i]);
            for (const x in flatObject) {
                if (!flatObject.hasOwnProperty(x)) continue;

                toReturn[i + '.' + x] = flatObject[x];
            }
        } else {
            toReturn[i] = ob[i];
            toReturn['*'] = (('*') in toReturn ? toReturn['*'] + ' ' : '') + String(ob[i]);
        }
    }
    return toReturn;
    */
}
exports.flattenObject = flattenObject;
//# sourceMappingURL=flattenObject.js.map