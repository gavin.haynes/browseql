"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const lodash = __importStar(require("lodash"));
const browseql_1 = require("./browseql");
const flattenObject_1 = require("./flattenObject");
function createFilter(query) {
    return createFilterFromAst(browseql_1.parse(query));
}
exports.createFilter = createFilter;
function tokenMatch(fieldToken, queryToken) {
    return fieldToken.toLowerCase().startsWith(queryToken.toLowerCase());
}
function createFilterFromAst(ast) {
    return (record) => {
        if (ast.op === "text") {
            const flatRecord = flattenObject_1.flattenObject(record);
            for (const fieldValue of lodash.values(flatRecord)) {
                const fieldTokens = new String(fieldValue).split(" ");
                for (const token of fieldTokens) {
                    if (tokenMatch(token, ast.value)) {
                        return true;
                    }
                }
            }
            return false;
        }
        if (ast.op === "eq") {
            function filterEq(fieldTokens, eqTo) {
                if (eqTo.op === "text") {
                    for (const token of fieldTokens) {
                        if (tokenMatch(token, eqTo.value)) {
                            return true;
                        }
                    }
                    return false;
                }
                if (eqTo.op === "or") {
                    for (const arg of eqTo.args) {
                        if (filterEq(fieldTokens, arg)) {
                            return true;
                        }
                    }
                    return false;
                }
                if (eqTo.op === "and") {
                    for (const arg of eqTo.args) {
                        if (!filterEq(fieldTokens, arg)) {
                            return false;
                        }
                    }
                    return true;
                }
            }
            const flatRecord = flattenObject_1.flattenObject(record);
            const fieldValue = flatRecord[ast.field];
            const fieldTokens = new String(fieldValue).split(" ");
            return filterEq(fieldTokens, ast.value);
        }
        if (ast.op === "or") {
            const filters = ast.args.map(createFilterFromAst);
            for (const filter of filters) {
                if (filter(record)) {
                    return true;
                }
            }
            return false;
        }
        if (ast.op === "and") {
            const filters = ast.args.map(createFilterFromAst);
            for (const filter of filters) {
                if (!filter(record)) {
                    return false;
                }
            }
            return true;
        }
        return false;
    };
}
//# sourceMappingURL=index.js.map