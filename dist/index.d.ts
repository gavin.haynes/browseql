interface Record {
    [key: string]: any;
}
export declare function createFilter(query: string): (record: Record) => boolean | undefined;
export {};
