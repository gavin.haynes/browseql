# Browseql

A simple text filtering library for web clients

## Usage

`npm install --save 'gitlab:gavin.haynes/browseql'`

```
const browseql = require('browseql');

const filter = browseql.createFilter("nice");

filter({"x": "things that are nice"});
// true

filter({"x": "bad things"});
// false
```

See more examples under `test`

## Details

Query syntax:

* token
* query1 or query2
* (query1) and (query2)
* key = value
* key = (value1 or value2 and value3)

Text is tokenized by spaces.

A query token matches a text token if the query token is a prefix of the text token without respect to case.
