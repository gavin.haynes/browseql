import * as assert from "assert";

import { Query } from "../src/query";
import { createFilter } from "../src/index";

const data = [
  { "abc": "a", "xyz": "x", "123": 1 },
  { "abc": "b", "xyz": "y", "123": 2 },
  { "abc": "c", "xyz": "z", "123": 3 }
];

describe('browseql', function() {
  it('should match with field agnostic queries', function() {
    assert.equal(createFilter("a")(data[0]), true);
    assert.equal(createFilter("3")(data[2]), true);

    assert.equal(createFilter("z")(data[1]), false);
    assert.equal(createFilter("1")(data[2]), false);
  });

  it('should match with disjunctive and conjunctive queries', function() {
    assert.equal(createFilter("a & 1")(data[0]), true);
    assert.equal(createFilter("b & 2")(data[1]), true);
    assert.equal(createFilter("c | 3")(data[2]), true);

    assert.equal(createFilter("b | 3")(data[0]), false);
    assert.equal(createFilter("z & z")(data[1]), false);
    assert.equal(createFilter("1 & 2")(data[2]), false);
  });

  it('should match with field specific queries', function() {
    assert.equal(createFilter("a = x")({"a": "john x"}), true);
    assert.equal(createFilter("a = (x & y)")({"a": "x y"}), true);
    assert.equal(createFilter("a = (x & (y | z))")({"a": "x y"}), true);
    assert.equal(createFilter("a = (x | y)")({"a": "x"}), true);

    assert.equal(createFilter("a = y")({"a": "x"}), false);
    assert.equal(createFilter("b = x")({"a": "x"}), false);
  });

  it('should match with nested queries', function() {
    assert.equal(createFilter("(a = x) | y")({"a": "x", "b": "y"}), true);
    assert.equal(createFilter("a = x | b = x")({"a": "x", "b": "y"}), true);
    assert.equal(createFilter("a = y | b = y")({"a": "x", "b": "y"}), true);
    assert.equal(createFilter("a = x & b = y")({"a": "x", "b": "y"}), true);
  });

  it('should be doing prefix searches', function() {
    assert.equal(createFilter("br")({"x": "brace"}), true);
    assert.equal(createFilter("ace")({"x": "brace"}), false);
  });

  it('should ignore case', function() {
    assert.equal(createFilter("BR")({"x": "brace"}), true);
    assert.equal(createFilter("ACE")({"x": "brace"}), false);
  });
  it('should handle escape sequenses', function() {
    assert.equal(createFilter("\"\\\\\"")({"x": "\\"}), true);
    assert.equal(createFilter("\"abc\"")({"x": "abc"}), true);
    assert.equal(createFilter("\"\\\"abc\\\"\"")({"x": "abc"}), false);
    assert.equal(createFilter("\"a\\\\\\\"bc\"")({"x": "a\\\"bc"}), true);
  });
  /*
  it('should handle nested values', function() {
    assert.equal(createFilter("x.y = z")({"x": {"y": "z"}}), true);
    assert.equal(createFilter("x.y = z")({"x": {"y": "q"}}), false);
    assert.equal(createFilter("x.y.* = a")({"x": {"y": [ "z", "a", "b" ]}}), true);
    assert.equal(createFilter("x.y.* = y")({"x": {"y": [ "z", "a", "b" ]}}), false);
  });
  it('should work with some janky values', function() {
    assert.equal(createFilter("x.y = z")({"x": {"y": "z", "r": Symbol("b")}}), true);
    assert.equal(createFilter("x.r = null")({"x": {"y": "z", "r": null}}), true);
  });
  */
});


